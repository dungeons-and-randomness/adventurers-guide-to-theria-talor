# Changelog

## 0.1.0 - 2024-6-26

### New Compendium Items

- Balance Domain
- Beastly Domain
- Chaos Domain
- Emotion Domain
- Fear Domain
- Fluctuation Domain
- Merriment Domain
- Morality Domain
- Natural Domain
- Perpetual Domain
- Violence Domain
- Willpower Domain
- Wisdom Domain

## 0.0.2 - 2024-6-26

### New Compendium Items

- Blood Mage class and subclasses
- Remaining Blood Magic spells

## 0.0.1 - 2024-6-25

### New Compendium Items

- Blood Magic spells up to level 2