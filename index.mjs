Hooks.once("init", () => {
  // Make blood magic a valid spellcasting school
  CONFIG.DND5E.spellSchools.blood = {
    label: "AGTT.SchoolBlood",
    icon: "modules/adventurers-guide-to-theria-talor/icons/blood-magic.svg",
    fullKey: "blood",
    reference: ""
  };

  // Adds AGTT as a suggested book when you define the source of an item
  CONFIG.DND5E.sourceBooks.AGTT = "Adventurer's Guide to Theria - Ta'lor"
});