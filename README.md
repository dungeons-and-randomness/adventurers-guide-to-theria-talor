# Adventurer's Guide to Theria - Ta'lor

Content from The Adventurer's Guide to Theria Volume 2 adapted for use with FoundryVTT.

## Included Content

- 1 new class
- 1 new school of magic
- 13 new cleric domains